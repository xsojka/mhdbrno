import csv
from typing import List, Tuple, Any, Dict, Optional, Union
import heapq

Seconds = int
StopId = str
Time = int
TripId = str
Departure = Tuple[Time, TripId]
Moment = Tuple[Time, StopId, StopId]  # one moment in spacetime. Last StopId is origin stop
stop_times_by_tripid: Dict[TripId, List["StopTime"]] = dict()
trips = dict()
stops = dict()
stops_by_canonical_id: Dict[StopId, List[StopId]] = dict()

###### misc

def parse_time(inp: str) -> Time:
    '''Parse time from 12:34:00 into amount of seconds since midnight'''
    # strftime to parse
    parts = inp.split(":")
    if len(parts) != 3:
        print(inp)
        assert len(parts) == 3
    res = 0
    res += int(parts[0]) * 60 * 60
    res += int(parts[1]) * 60
    res += int(parts[2])
    return res

###### classes

'''Holds information about one Trip of a vehicle from its start stop to its terminus.'''
class Trip:
    def __init__(self, trip_id: str, line):
        self.trip_id = trip_id
        self.route_id = line['route_id']
        self.trip_headsign = line['trip_headsign']
        self._stops: Optional[List[StopTime]] = None
    
    @property
    def stops(self):
        '''Get a list of StopTimes this Trip stops at. Generated just in time, only if needed. Access with trip.stops'''
        # yes this is inefficient and i know it
        if self._stops is not None:
            return self._stops
        self._stops = []
        for stop_time in stop_times_by_tripid:
            if stop_time.trip_id == self.trip_id:
                self._stops.append(stop_time)


class StopTime:
    def __init__(self, trip_id: str, arrival_time: str, departure_time: str, stop_id: str, stop_sequence: str) -> None:
        self.trip_id = trip_id
        self.arrival_time = parse_time(arrival_time)
        self.departure_time = parse_time(departure_time)
        self.stop_id = stop_id
        self.stop_sequence = int(stop_sequence)


class Stop:
    def __init__(self, stop_id: str, stop_name: str, parent_station: str) -> None:
        self.stop_id = stop_id
        self.stop_name = stop_name
        self.parent_station = parent_station
        if parent_station not in stops_by_canonical_id:
            stops_by_canonical_id[parent_station] = [stop_id]
        else:
            stops_by_canonical_id[parent_station].append(stop_id)
    
    def get_canonical(self) -> "Stop":
        if len(self.parent_station) > 0:
            return stops[self.parent_station]
        else:
            return self
    
    def __str__(self) -> str:
        return self.stop_name

###### loading

def load_file(filename: str):
    with open(filename) as file:
        reader = csv.DictReader(file, quotechar='"', delimiter=",")
        return list(reader)  # you can read from a reader only once, since it's only a pointer in the csv file.
        # if you want to iterate through it multiple times, return list(reader).


def load_trips(filename: str) -> Dict[TripId, Trip]:
    with open(filename) as file:
        trips_raw = csv.DictReader(file, quotechar='"', delimiter=",")
        res = dict()
        for trip_raw in trips_raw:
            trip_raw['route_id'] = trip_raw['\ufeffroute_id']
            del trip_raw['\ufeffroute_id']
            trip = Trip(trip_raw['trip_id'], trip_raw)
            res[trip_raw['trip_id']] = trip
        return res


def load_stop_times(filename: str) -> Tuple[Dict[TripId, List["StopTime"]], Dict[StopId, List["StopTime"]]]:
    '''
    @returns tripId -> stopTime, canonical stopId -> stopTime
    '''
    with open(filename) as file:
        stop_times_raw = csv.DictReader(file, quotechar='"', delimiter=",")
        res = dict()
        res_stopid = dict()
        for raw in stop_times_raw:
            stop_time = StopTime(raw['\ufefftrip_id'], raw['arrival_time'], raw['departure_time'], raw['stop_id'], raw['stop_sequence'])
            if raw['\ufefftrip_id'] in res:
                res[raw['\ufefftrip_id']].append(stop_time)
            else:
                res[raw['\ufefftrip_id']] = [stop_time]
            if raw['stop_id'] in res_stopid:
                res_stopid[raw['stop_id']].append(stop_time)
            else:
                res_stopid[raw['stop_id']] = [stop_time]
        return res, res_stopid


def load_stops(filename: str) -> Dict[StopId, "Stop"]:
    with open(filename) as file:
        stops_raw = csv.DictReader(file, quotechar='"', delimiter=",")
        res = dict()
        for raw in stops_raw:
            stop = Stop(raw['\ufeffstop_id'], raw['stop_name'], raw['parent_station'])
            res[raw['\ufeffstop_id']] = stop
        return res


trips = load_trips("gtfs/trips.txt")
stops = load_stops("gtfs/stops.txt")
stop_times_by_tripid, stop_times_by_stopid = load_stop_times("gtfs/stop_times.txt")

###### niceties

def human_readable_time(secs_since_midnight: Time) -> str:
    hours = str(secs_since_midnight // (60 * 60))
    mins = str((secs_since_midnight % (60 * 60)) // 60)
    secs = str(secs_since_midnight % 60)
    return f'{hours.zfill(2)}:{mins.zfill(2)}:{secs.zfill(2)}'

def human_readable_visit(moment: Tuple[StopId, Tuple[StopId, Time]]) -> str:
    stopid, o = moment
    origin, secs_since_midnight = o
    return f'{stops[stopid]} at {human_readable_time(secs_since_midnight)} from {stops[origin]}'

def human_readable_stop(stopid: str) -> str:
    return str(stops[stopid])


###### core functionality

def leaves_from_stop(stop_id: StopId, currtime: Time, max_wait=20*60) -> List[Departure]:
    res: List[Departure] = []
    canonical_stop_id = stops[stop_id].get_canonical().stop_id
    for stop_id in stops_by_canonical_id[canonical_stop_id]:
        for stop_time in stop_times_by_stopid.get(stop_id, []):
            if stop_time.departure_time > currtime and \
                max_wait + currtime < stop_time.departure_time:
                new = stop_time.departure_time, stop_time.trip_id
                res.append(new)
    return res

# get all possible disembarkements from a connection you jumped on
# origin is only for path tracking
def get_arrivals_from_departure(departure: Departure, origin: StopTime) -> List[Moment]:
    departure_time, trip_id = departure
    res: List[Moment] = []
    for stop_time in stop_times_by_tripid[trip_id]:
        if stop_time.arrival_time > departure_time:
            res.append(tuple([stop_time.arrival_time, stop_time.stop_id, origin]))
    return res


def flood_fill(stop_id: str, start_time: Time, end_time: Time) -> Dict[StopId, Tuple[StopId, Time]]:
    stop_id = stops[stop_id].get_canonical().stop_id
    curr_moment = start_time, stop_id, stop_id
    heap = [curr_moment]
    reachable = set()
    currtime, _, _ = heap[0]
    visited: Dict[StopId, Tuple[StopId, Time]] = dict() # visited[stop] = from, time
    while currtime <= end_time and len(heap) > 0:
        currtime, currstop, oldstop = heapq.heappop(heap)
        currstop = stops[currstop].get_canonical().stop_id
        if (currstop) in visited:
            continue
        visited[canonical(currstop)] = (canonical(oldstop), currtime)
        departures = leaves_from_stop(currstop, currtime, currtime - end_time)
        for departure in departures:
            moments = get_arrivals_from_departure(departure, currstop)
            for moment in moments:
                heapq.heappush(heap, moment)
    return visited


def get_reachable_from_stop(stop_id: str, start_time: Union[Time, str], end_time: Union[Time, str]) -> List[str]:
    if isinstance(start_time, str):
        start_time = parse_time(start_time)
    if isinstance(end_time, str):
        end_time = parse_time(end_time)
    visited = flood_fill(stop_id, start_time, end_time)
    return list(map(human_readable_visit, visited.items()))

def canonical(stopid: str) -> str:
    return stops[stopid].get_canonical().stop_id

def get_route(start_stop_id: str, end_stop_id: str, start_time: Union[int, str], max_time=2*60*60):
    if isinstance(start_time, str):
        start_time = parse_time(start_time)
    visited = flood_fill(start_stop_id, start_time, start_time+max_time)
    path = []
    currstop = end_stop_id
    while currstop != start_stop_id:
        oldstop = currstop
        currstop, time = visited[canonical(currstop)]
        path.append((oldstop, (currstop, time)))
    return path

Arbesova = 'U1008N3122'
Olbrachtovo = "U1453Z1"
Moravské = "U1397N2846"
r = get_route(Moravské, Olbrachtovo, "22:08:00")
print(list(map(human_readable_visit, r)))